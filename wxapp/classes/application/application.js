const my = require('../lib/util.js');
const demo = require('_demo.js');
const api = require('_api.js');
module.exports = {
  init: function (json) {
    my.request({
      url: api.application_init,
      params : json.params,
      success: json.callback,
      error :json.error
    });
  },
  speedBarClose : function(page){
    page.setData({
      speedBarStatus: 0,
      speedBarOpenStayle: "my-speedbar-close"
    });
  },
  speedBarOpen: function (page) {
    page.setData({
      speedBarStatus: 1,
      speedBarOpenStayle: "my-speedbar-open"
    });
  },
  relogin : function(){
    var app = getApp();
    app.globalData.step = 1;
    wx.redirectTo({
      url: '../index/index',
    });
  },
  send_verfiy_code : function(json){
    my.request({
      url: api.send_verfiy_code,
      params: {
        uid : json.uid,
        mobile : json.mobile,
        verify_code : json.code
      },
      success: json.callback,
      error: json.error
    });
  },
}