const my = require('../lib/util.js');
const pager = require('../lib/pager.js');
const demo = require('_demo.js');
const api = require('_api.js');
//用户登录
module.exports = {
  list: function (json) {
    my.request({
      url: api.board_messages,
      params: {
        page: pager.p(json.page),
        size: pager.ps(json.pageSize)
      },
      success: json.callback,
    });
  },
  details :function(json){
    my.request({
      url: api.board_view,
      params: {
        id : json.id
      },
      success: json.callback,
    });
  },
}