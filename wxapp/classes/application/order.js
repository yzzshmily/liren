const my = require('../lib/util.js');
const pager = require('../lib/pager.js');
const demo = require('_demo.js');
const api = require('_api.js');
module.exports = {
  prepay : function (json) {
    my.request({
      url: api.unifiedorder,
      params: json.params,
      success: json.success
    });
  },
  create_first: function (json) {
    const app = getApp();
    my.request({
      url: api.order_join,
      params:{
        uid: json.uid,
        "type": 0,
        amount: json.amount,
        address: json.address_id,
      },
      success: json.callback,
    });
  },
  create_more: function (json) {
    const app = getApp();
    my.request({
      url: api.order_join,
      params: {
        uid: json.uid,
        "type": 1,
        amount: json.amount,
        address: json.address_id,
      },
      success: json.callback,
    });
  },
  list : function(json){
    my.request({
      url: api.order_list,
      params: {
        uid: json.uid,
        oid: json.openid,
        page: pager.p(json.page),
        size : pager.ps(json.pageSize),
      },
      success: json.callback,
    });
  },
  parseOrderStatusText : function(os){
    return (os==0)?"未确认":"已确认";
  },
  parsePayStatusText: function (ps) {
    if(ps==0){
      return "未付款";
    }else if(ps==1){
      return "付款中";
    }else{
      return "已付款";
    }
  },
  parse : function(di){
    di.order_status_text = this.parseOrderStatusText(di.order_status);
    di.pay_status_text = this.parsePayStatusText(di.pay_status);
    di.goods = (di.goods.length > 0) ? di.goods[0] : di.goods;
    return di;
  },
  parseList : function(list){
    for(var i=0;i<list.length;i++){
      list[i] = this.parse(list[i]);
    }
    return list;
  }
}