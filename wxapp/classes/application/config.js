const my = require('../lib/util.js');
const demo = require('_demo.js');
const api = require('_api.js');
module.exports = {
  banks : function (json) {
    my.request({
      url: api.config_banks,
      params: {},
      success: json.callback,
    });
  },
  //配置区域列表
  regions: function (json) {
    my.request({
      url: api.config_regions,
      params: {},
      success: json.callback,
    });
  },
  games: function (json) {
    my.request({
      url: api.config_games,
      params: {},
      success: json.callback,
    });
  },
}