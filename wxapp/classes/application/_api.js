const consts = require('../consts.js');
const u = function (p) {
  const app = getApp();
  if (app.globalData.scene == '1017') {
    return consts.API_URI_TEST + p;
  } else {
    return consts.API_URI + p;
  }
};
module.exports = {
  //access2token
  get access2token() { return u("wechat/access2token"); },
  //code2session
  get code2session() { return u("wechat/code2session"); },
  //用户登录初始化
  get application_init() {return u("application/init"); },
  //微信统一下单
  get unifiedorder() { return u("wechat/unifiedorder"); },
  //地址管理保存
  get address_save() { return u("address/save"); },
  //地址管理列表
  get address_mine() { return u("address/mine"); },
  //地址管理删除
  get address_delete() { return u("address/delete"); },
  //订单创建新单
  get order_join() { return u("order/join"); },
  //订单状态列表
  get order_status() { return u("order/status"); },
  //配置区域列表
  get config_regions() { return u("config/regions"); },
  //配置银行列表
  get config_banks() { return u("config/banks"); },
  //配置游戏列表
  get config_games() { return u("config/games"); },
  //提现管理撤回
  get account_revoke() { return u("account/revoke"); },
  //提现管理列表
  get account_orders() { return u("account/orders"); },
  //提现管理申请
  get account_withdraw() { return u("account/withdraw"); },
  //消息查看详情
  get board_view() { return u("board/view"); },
  //消息公告列表
  get board_messages() { return u("board/messages"); },
  /** 用户登录注册 */
  get user_login() { return u("user/login"); },
  //用户推荐列表
  get user_referrals() { return u("user/referrals"); },
  //用户账户列表
  get user_all() { return u("user/all"); },
  //用户账户详情
  get user_view() { return u("user/view"); },
  //游戏排行榜
  get game_top() { return u("game/top"); },
  //游戏排行总榜
  get game_top24() { return u("game/top24"); },
  //加速记录
  get game_shift_history() { return u("game/shiftHistory"); },
  //全站加速记录
  get game_shift() { return u("game/shiftTop"); },
  //小程序二维码
  get wechat_qrcode() { return u("wechat/qrcode"); },
  //模版消息
  get wechat_template() { return u("wechat/template"); },
  //更改用户角色性别
  get role_gender() { return u("game/roleGender"); },
  //商品详情
  get goods_details() { return u("goods/from"); },

  //楚兰帐号登录
  get chulan_passport() { return u("chulan/passport"); },

  //订单列表
  get order_list() { return u("order/mine"); },

  //发送短信验证码
  get send_verfiy_code() { return u("sms/code"); },

  //更新手机号码和密码
  get user_bind() { return u("user/password"); },
}
