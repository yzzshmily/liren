const my = require('../lib/util.js');
const pager = require('../lib/pager.js');
const demo = require('_demo.js');
const api = require('_api.js');
module.exports = {
  mine: function (json) {
    my.request({
      url: api.address_mine,
      params:{
        oid: json.openid,
        page: pager.p(json.page),
        size: pager.ps(json.pageSize),
      },
      success: json.callback
    })
  },
  save: function (json) {
    my.request({
      url: api.address_save,
      params: {
        oid: json.openid,
        order:json.order_id,
        id : json.address_id,
        country: json.country,
        province: json.province,
        city: json.city,
        district: json.district,
        consignee: json.consignee,
        address: json.address,
        zipcode: json.zipcode,
        tel: json.tel,
      },
      success: json.callback
    })
  },
  region: function(json){
    my.request({
      url: api.config_regions,
      params: {
        'type': json.type,
        "parent": json.parent
      },
      success: json.callback
    })
  },
  remove: function(json){
    my.request({
      url: api.address_delete,
      params: {
        oid: json.openid,
        address: json.address_id
      },
      success: json.callback
    })
  },
}