const my = require('../lib/util.js');
const consts = require('../consts.js');
const pager = require('../lib/pager.js');
const game = require('game.js');
const demo = require('_demo.js');
const api = require('_api.js');
//用户登录
module.exports = {
  login : function(json,callback) {
    my.request({
      url: api.user_login,
      loading: false,
      params: {
        oid: json.openid,
        rid: json.from_user_id,
        unionid: json.unionid,
        scene: json.scene,        
        nick_name: json.userInfo.nickName,
        sex: json.userInfo.gender,
        province: json.userInfo.province,
        city: json.userInfo.city,
        country: json.userInfo.country,
        language: json.userInfo.language,
        is_subscribe: json.userInfo.is_subscribe,
        lng: json.userInfo.lng,
        lat: json.userInfo.lat,
        mobile: json.userInfo.mobile,
        head_pic: json.userInfo.avatarUrl,
      },
      success : function(data){
        if(typeof(callback)=="function"){
          callback(data);
        }
      },
    });
  },
  bind :function(json){
    my.request({
      url: api.user_bind,
      loading:false,
      params: {
        oid: json.openid,
        mobile : json.mobile,
        password : json.password,
      },
      success: json.callback,
    }); 
  },
  //账户详情
  details : function(json){
    my.request({
      url: api.user_view,
      loading:false,
      params: {
        uid: json.uid,
      },
      success: json.callback,
    }); 
  },
  user_all: function (json) {
    my.request({
      url: api.user_all,
      params: {
        oid: json.openid,
        task : json.task,
        level: json.level,
        page: pager.p(json.page),
        size: pager.ps(json.pageSize),
      },
      success: json.callback,
    });
  },
  user_referrals : function(json){
    my.request({
      url: api.user_referrals,
      params: {
        uid: json.uid,
        page : json.page,
        size : json.pageSize
      },
      success: json.callback,
    }); 
  },
  //账户提现申请
  withdraw : function (json) {
    my.request({
      url: api.account_withdraw,
      params: {
        oid: json.openid,
        bank:json.bank_code,
        account:json.account,
        amount: json.amount,
        name: json.real_name,
        address : json.address,
        note : ''
      },
      success: json.callback,
    });
  },
  //账户提现撤销
  withdrawCancel : function(json){
    my.request({
      url: api.account_revoke,
      params: {
        oid: json.openid,
        id: json.order_id,
      },
      success: json.callback,
    });
  },
  //提现记录
  withdrawOrders : function(json){
    my.request({
      url: api.account_orders,
      params: {
        uid: json.uid,
        "status": json.status,
        "page": json.page,
        "size": json.pageSize
      },
      success: json.callback,
    }); 
  },
  //更改角色性别
  role_gender : function(json){
    my.request({
      url: api.role_gender,
      params: {
        uid : json.uid,
        sex : json.sex
      },
      success: json.callback,
    }); 
  },
  check : function(){
    const app = getApp();
    this.details({
      uid: app.globalData.game.uid,
      callback: function (data) {
        var cur_task = my.ival(app.globalData.game.task);
        var cur_level = my.ival(app.globalData.game.level);
        var new_task = my.ival(data.game.task);
        var new_level = my.ival(data.game.level);
        if(
          cur_task!=new_task || cur_level!=new_level
        ){
          console.log("--------------------------------------");
          console.log("user:check:changed,goto navigate");
          console.log("cur task:" + cur_task);
          console.log("new task:" + new_task);
          console.log("cur level:" + cur_level);
          console.log("new level:" + new_level);
          console.log("--------------------------------------");
          wx.reLaunch({
            url: "../navigate/navigate"
          });
        }else{
          console.log("user:check:nothing change");
        }
      }
    });  
  },
  navigate: function (page,handler){
    wx.reLaunch({
      url: "../navigate/navigate"
    });
    return;
    console.log("user:navigate:invoke");
    const app = getApp();
    var uid = app.globalData.game.uid;
    console.log("user:navigate:uid=" + uid);
    this.details({
      uid: uid,
      callback: function (data) {
        console.log("user:navigate:game="+JSON.stringify(data.game));
        handler(data.game);
        wx.reLaunch({
          url: my.appendOptions(game.homePath(data.game), page.data.options)
        });
      }
    });    
  },
  refresh: function (page,handler) {
    var app = getApp();
    if (page.data.__userRefreshStop){
      console.log("user:refresh:__userRefreshStop is true,return");
      return;
    }
    var that = page;
    var self = this;
    this.details({
      uid: app.globalData.game.uid,
      callback: function (data) {
        if (data.game.level > app.globalData.game.level) {
          var _game = that.data.game;
          _game.balance = _game.goal;
          _game = game.parse(_game);
          that.setData({
            user: data,
            game: _game,
          });
          self.refreshStop(that);
          console.warn("======== user:refresh:game:balance ========");
          console.warn("level update:cur level=" + app.globalData.game.level);
          console.warn("user:refresh:game:uid=" + _game.uid);
          console.warn("user:refresh:game:balance=" + _game.balance);
          console.warn("user:refresh:game:task=" + _game.task);
          console.warn("user:refresh:game:level=" + _game.level);
          console.warn("user:refresh:game:restGoal=" + _game.restGoal);
          console.warn("-------------------------------------------");
          if(handler){
            handler(_game);
          }
        } else {
          var _game = game.parse(data.game);
          console.log("user:refresh:game:balance="+_game.balance);
          console.log("user:refresh:game:task=" + _game.task);
          console.log("user:refresh:game:level=" + _game.level);
          that.setData({
            user: data,
            game: _game,
          });
          setTimeout(function(){
            self.refresh(that);
            },consts.REFRESH_INTERVAL);
        }
      }
    });
  },
  refreshStart : function(page){
    page.setData({
      __userRefreshStop: false,
    });
    this.refresh(page);
  },
  refreshStop : function(page){
    page.setData({
      __userRefreshStop : true,
    });
  }
}