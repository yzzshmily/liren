const my = require('../lib/util.js');
const consts = require('../consts.js');
const pager = require('../lib/pager.js');
const demo = require('_demo.js');
const api = require('_api.js');
module.exports = {
  top : function (json) {
    my.request({
      url: api.game_top,
      params: {
        uid : json.uid,
        page : pager.p(json.page),
        size : pager.ps(json.pageSize),
      },
      success: json.callback
    });
  },
  top24: function (json) {
    my.request({
      url: api.game_top,
      params: {
        page: pager.p(json.page),
        size: pager.ps(json.pageSize),
      },
      success: json.callback
    });
  },
  shift_top: function (json) {
    my.request({
      url: api.game_shift,
      loading: json.loading,
      params: {
        page: pager.p(json.page),
        size: pager.ps(json.pageSize),
      },
      success: json.callback
    });
  },
  shift_history : function(json){
    console.log("---------------------page="+json.page);
    my.request({
      url: api.game_shift_history,
      loading:json.loading,
      params: {
        oid:(json.openid)?json.openid:0,
        page: pager.p(json.page),
        size: pager.ps(json.pageSize),
      },
      success: json.callback
    });    
  },
  refreshSpeed:function(page){
    if (page.data.__refreshSpeedStop){
      console.log("game:refreshSpeed:__refreshSpeedStop is true,return");
      return;
    }
    var that = page;
    var self = this;
    this.shift_history({
      loading: false,
      openid: 0,
      page: 0,
      pageSize: 1,
      callback: function (data) {
        var speedlog = (data.data && data.data.length > 0) ? data.data[0] : null;
        that.setData({
          speedlog: speedlog
        });
        setTimeout(function(){
          self.refreshSpeed(that);
          }, consts.REFRESH_INTERVAL);
      }
    });    
  },
  refreshSpeedStart : function(page){
    page.setData({
      __refreshSpeedStop: false
    });
    this.refreshSpeed(page);
  },
  refreshSpeedStop: function (page) {
    page.setData({
      __refreshSpeedStop : true
    });
  },
  refreshCostTime: function (page) {
    if (page.data.game && page.data.game.restGoal>0){
      page.setData({
        costTimeText: this.parseCostTime(page.data.game.dateline)
      });
    }else{
      return;
    }
    var self = this;
    var that = page;
    setTimeout(function(){
      self.refreshCostTime(that);
      }, 33);
  },
  parseBalance: function (balance) {
    var t = balance + '';
    t = t.split('.')[0];
    var max = 8;
    var last = max - t.length;
    var tail = '';
    for (var i = 0; i < last; i++) {
      tail += '0';
    }
    t = tail + t;
    return t;
  },
  parseCostShortTime:function(ts){
    var cur = (new Date()).getTime();
    var cost = cur - ts * 1000;
    var t = Math.ceil(cost/(1000));
    if(t<60){
      return t+"秒";
    }
    t = Math.ceil(cost / (60 * 1000));
    if (t < 60) {
      return t + "分钟";
    }
    t = Math.ceil(cost / (3600 * 1000));
    if (t < 24) {
      return t + "小时";
    }
    t = Math.ceil(cost / (24*3600 * 1000));
    return t + "天";
  },
  parseCostTime: function (ts) {
    var cur = (new Date()).getTime();
    var cost = cur-ts*1000;
    var h = Math.floor(cost / 3600000);
    cost = cost % 3600000;
    var min = Math.floor(cost/60000);
    if(min<10){
      min = "0"+min;
    }
    cost = cost % 60000;
    var sec = Math.floor(cost/1000);
    if (sec < 10) {
      sec = "0" + sec;
    }
    var ms = cost % 1000;
    if (ms < 100) {
      ms = "0" + ms;
    }else if(ms<10){
      ms = "00" + ms;
    }
    return [h,min,sec,ms].join('：');
  },
  parse : function(game){
    if(!game){
      console.log("game:parse:game is null");
      return game;
    }
    game.balanceText = this.parseBalance(game.balance);
    game.regDateText = my.formatDate(new Date(game.dateline*1000));
    game.regTimeText = my.formatTime(new Date(game.dateline * 1000));
    game.topTimeText = this.parseCostTime(new Date(game.rankTimestamp*1000));
    game.costTimeText = this.parseCostTime(game.rankTimestamp);
    game.costTimeShortText = this.parseCostShortTime(game.rankTimestamp);
    game.restGoal = my.fval(game.goal)-my.fval(game.balance);
    game.balanceInt = my.ival(game.balance);
    game.goal = my.ival(game.goal);
    if(game.restGoal<0){
      game.restGoal = 0;
    }
    return game;
  },
  parseUserList : function(ul,curUserId){
    if(!ul || ul.length<1){
      return [];
    }
    var list = [],di=null,cur=null;
    for(var i=0;i<ul.length;i++){
      di = ul[i];
      di.game = this.parse(di.game);
      if (di.game && di.game.uid == curUserId){
        console.log("game:parseUserList:find curUserId="+curUserId);
        cur = di;
      }else{
        list.push(di);
      }
    }
    if(cur){
      list.unshift(cur);
    }
    return list;
  },
  homePath : function(game){
    const app = getApp();
    //系统关闭
    if (app.globalData.cfg.app.system_offline == 1) {
      wx.redirectTo({
        url: '../index/index',
      });
      return;
    }
    var path = '';
    if (consts.FOR_FIRST_AUDIT){
      console.log("game:homePath:FOR_FIRST_AUDIT=" + consts.FOR_FIRST_AUDIT);
      if(game.order<1){
        path = '../signin/signin';
      } else if (game.task == 0) {
        path = '../pay/pay';
      } else if (game.task >= 1) {
        path = "../order/order";
      }
      console.log("game:homePath:FOR_FIRST_AUDIT path=" + path);
      return path;
    }else{
      console.log("game:homePath:FOR_FIRST_AUDIT=" + consts.FOR_FIRST_AUDIT);
    }
    //抢花魁24小时活动
    if (app.globalData.cfg.app.opening_24_hour == 1) {
      console.log("game:homePath:opening_24_hour=1");
      if (game.task >= 1){
        path = '../h24waiting/h24waiting';
        return path;
      }else if(game.order<1){
          path = '../h24signin/h24signin';
          return path;
      }else{
        path = '../h24start/h24start';
        return path;
      }
    }

    if (game.order < 1) {
      path = '../signin/signin';
    } else if (game.task == 0) {
      path = '../start/start';
    } else if (game.task == 1) {//买完东西，没进入队列
      path = '../share/share';
    } else if (game.task == 2) {//进入队列
      path = '../shareMore/shareMore';
    } else if (game.task == 3) {
      path = '../speedup/speedup';
    } else if (game.task == 4) {
      if (game.balance == 0) {
        path = "../index/index";
      } else {
        path = "../user/user";
      }
    }
    console.log("game:homePath:path="+path);
    return path;
  },
  share3usersList: function (shareList, realReferrals){
    if(arguments.length==1){
      realReferrals = 0;
    }
    var shareText = [
      [
        "来吧！推荐好友，把我点亮！",
        "未完成推荐",
        "未完成推荐"
      ],
      [
        "来吧！推荐好友，把我点亮！",
        "真棒！来把我也点亮吧！",
        "未完成推荐"
      ],
      [
        "",
        "",
        "就差最后一个了！"
      ],
    ];
    var di = null;
    var users = this.parseUserList(shareList);
    var list = [];
    for (var i = 0; i < 3; i++) {
      if (i < users.length) {
        di = users[i];
        di.isuser = true;
        if (!di.headPic || di.headPic == '') {
          di.headPic = consts.DEF_AVATAR_ACTIVE;
        }
        list.push(di);
      } else if (i < realReferrals) {
        list.push({
          isuser: false,
          headPic: consts.DEF_AVATAR_ACTIVE,
          notice: "有好友为您点亮！",
        })
      } else {
        list.push({
          isuser: false,
          headPic: consts.DEF_AVATAR,
          notice: shareText[users.length][i]
        })
      }
    }
    console.log("game:share3users:return list size="+list.length);
    return list;
  },
  pickerTask : function(){
    return [
      { value: -1, text: "全部进度" },
      { value: 0,  text:"未购买"},
      { value: 1, text: "已购买未激活" },
      { value: 2, text: "已激活" },
    ]
  },
  pickerLevel: function () {
    return [
      { value: 0, text: "全部等级" },
      { value: 1, text: "1级" },
      { value: 2, text: "2级" },
      { value: 3, text: "3级" },
      { value: 4, text: "4级" },
      { value: 5, text: "5级" },
    ]
  },
}