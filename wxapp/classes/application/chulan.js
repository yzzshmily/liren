const my = require('../lib/util.js');
const demo = require('_demo.js');
const api = require('_api.js');
module.exports = {
  passport: function (json) {
    my.request({
      url: api.chulan_passport,
      params: {
        username : json.username,
        password: json.password
      },
      success: json.callback,
      error : json.error,
    });
  },
}