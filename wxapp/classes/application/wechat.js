const my = require('../lib/util.js');
const demo = require('_demo.js');
const consts = require('../consts.js');
const api = require('_api.js');
const WXBizDataCrypt = require('../wx/RdWXBizDataCrypt.js');
module.exports = {
  bizDataCrypt : function (sessionKey, encryptedData, iv) {
    var appId = consts.APPID;
    console.log("wechat:bizDataCrypt:appId="+appId);
    console.log("wechat:bizDataCrypt:sessionKey=" + sessionKey);
    console.log("wechat:bizDataCrypt:iv=" + iv);
    var pc = new WXBizDataCrypt(appId, sessionKey);
    return pc.decryptData(encryptedData, iv);
  },
  accessToken : function(json){
    my.request({
      url: api.access2token,
      params: {},
      success: json.callback,
    });
  },
  getOpenid : function(json){
    my.request({
      url: api.code2session,
      loading: false,
      params:{
        app: json.app,
        code: json.code,
      },
      success: json.callback,
      error: json.error
    });
  },
  getQr : function(json){
    my.request({
      url: api.wechat_qrcode,
      params:{
        scene : json.scene,
        page : json.page,
      },
      success : json.callback,
      error : json.error
    });
  },
  tmsg : function(json){
    console.log("wechat:tmsg:json="+JSON.stringify(json));
    my.request({
      url: api.wechat_template,
      params: {
        touser: json.openid,
        template_id: json.template_id,
        form_id: json.form_id,
        data: json.data,
        page: json.page,
      },
      loading: true,
      success: json.callback,
      error: json.error
    });
  },

}