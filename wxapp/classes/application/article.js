const my = require('../lib/util.js');
const demo = require('_demo.js');
const api = require('_api.js');
module.exports = {
  view : function (json) {
    my.request({
      url: api.board_view,
      params: {
        title: json.title
      },
      success: json.callback,
    });
  },
  list : function (json) {
    my.request({
      url: api.board_messages,
      params: {
        title: json.title
      },
      success: json.callback,
    });
  }
}