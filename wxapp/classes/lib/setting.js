const setting = [
  {
    key : "ORDER_COIN",
    text : "每单消耗金币",
    val : 10,
  },
  {
    key: "CREATE_MIX_COIN",
    text: "创建订单需要的最少金币",
    val: 100,
  },
];
const init = function(setting){
  setting = setting;
};
var __data = null;
const data = function(){
  if(__data==null){
    __data = {};
    for (var i = 0; i < setting.length; i++) {
      __data[setting[i].key] = setting[i].val;
    }
  }
  return __data;
};
module.exports = {
  data: data,
  init: init,
  setting: setting,
}