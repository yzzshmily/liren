const my = require('util.js');
const instance = function(json){
  return {
    token: json.token,
    title: json.title,
    version: json.version
  };
};
module.exports = {
  instance: instance,
}