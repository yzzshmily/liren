const my = require("util.js");
const PAGE_SIZE = 30;
var __startIndex = 0;
var __pageSize = PAGE_SIZE;
var __currentPage = 0;
var __totalSize = 0;
var __total = 0;
var __hasNext = true;

const next = function(){
  __startIndex = __currentPage*__pageSize;
  __currentPage++;
  return __startIndex;
}
const page = function(page){
  if (arguments.length>0){
    __currentPage = page;
  }
  return __currentPage;
};

const pageSize = function (pageSize) {
  if (arguments.length > 0) {
    __pageSize = pageSize;
  }
  return __pageSize;
};
const text = function(){
  var text = "已加载 ";
  text += __totalSize;
  if(__total>0){
    text += "/"+__total;
  }
  text += " 条记录，";
  if(__hasNext){
    text += "查看更多";
  }else if(__totalSize==0){
    text += "没有要查找的数据";
  }else{
    text += "没有更多内容了";
  }
  return text;
};
const hasNext = function(dataSize,total){
  __totalSize += my.ival(dataSize);
  if(arguments.length==2){
    __total = my.ival(total);
  }
  console.log("pager:hasNext:dataSize="+dataSize);
  console.log("pager:hasNext:__pageSize=" + __pageSize);
  __hasNext = dataSize >= __pageSize;
  return __hasNext;
};
const init = function(pageSize,startIndex){
  __totalSize = 0;
  if(arguments.length==1){
    __pageSize = pageSize;
  }else{
    __pageSize = PAGE_SIZE;
  }
  if(arguments.length>1){
    __startIndex = startIndex;
  }else{
    __startIndex = 0;
    __currentPage = 0;
  }
};

const P = function(page){
  return (page && page>=0)?page:0;
};
const PS = function (pageSize) {
  return (pageSize && pageSize>1) ? pageSize : 1;
};

module.exports = {
  init: init,
  next: next,
  page: page,
  pageSize: pageSize,
  hasNext: hasNext,
  p : P,
  ps : PS,
  text:text,
}