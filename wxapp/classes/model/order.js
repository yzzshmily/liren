const STATUS = [
  {
    code: 10,//下单成功
    text: "待付款",
  },
  {
    code: 15,//等待支付回调确认成功
    text: "支付中",
  },
  {
    code: 20,//付款成功，等待发货
    text: "待发货",
  },
  {
    code: 30,//已发货，商品在途，等待买家收货
    text: "待收货",
  },
  {
    code: 40,//买家确认收货，订单完成
    text: "已完成",
  },
  {
    code: 50,
    text: "退款中",
  },
  {
    code: 55,//退款完成，订单关闭
    text: "退款完成",
  },
  {
    code: 60,
    text: "退货中",
  },
  {
    code: 60,//退货完成，订单关闭
    text: "退货完成",
  },
];
var status = function(code){
  for(var i=0;i<STATUS.length;i++){
    if(STATUS[I].code = code){
      return STATUS[I].text;
    }
  }
  return "未知状态";
};
module.exports = {
  STATUS: STATUS,
  status : status,
}