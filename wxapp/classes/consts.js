const APP_NAME = "蚂蚁升级";
const APP_CODE = "MYSJ";
const APP_SHARE_TITLE = "蚂蚁升级，好玩儿的分享购物游戏";
const APP_VERSION = "0.18071602";
const API_URI =  "https://mp-mysj.sanduapp.com/index.php?s=api/";
const API_URI_TEST = "https://cs-mp-mysj.sanduapp.com/index.php?s=api/";
const APPID = 'wxb70454b8bd5135c5';
//小程序支付使用的appid(使用小程序的APPID)
const PAY_APPID = "wxb70454b8bd5135c5";
//微信支付商户号
const PAY_MACH_ID = "1508810121";
//微信支付商户KEY
const PAY_KEY = "ADkjds67813HAKDV8793hvjv98y87sfd";
//最少提现金额
const MIN_WITHDRAW_AMOUNT = 50.00;
//最大单笔提现金额
const MAX_WITHDRAW_AMOUNT = 20000.00;
const ACTIVITY_USING_TYPE = {};//适用类型  0：通用 1：拓新客: 2：激活沉睡客户'
const ACTIVITY_DISPLAY_TYPE = {};//活动类型  0：通用 1：爆品  2：报名 3：缴费',

const FOR_FIRST_AUDIT = false;
const USE_UNIONID = false;
const REFRESH_INTERVAL = 5000;

//缺省的头像
const DEF_AVATAR = "http://mp-mysj.sanduapp.com/static/wxapp/avater/default.png";
const DEF_AVATAR_ACTIVE = "http://mp-mysj.sanduapp.com/static/wxapp/avater/default_a.png";

const TMSG_EXCHANGE = {
  id: "nlPV5e8eCiG-XUFDVczh0J-7KTSJpi8pZyjBUVeEZlw",
  title : "兑换成功通知",
  content : [
    "兑换类型{{ keyword1.DATA }}",
    "兑换价值{{ keyword2.DATA }}",
    "商家名称{{ keyword3.DATA }}",
    "会员昵称{{ keyword4.DATA }}",
    "兑换时间{{ keyword5.DATA }}"
  ]
};
const TMSG_ORDER_PAYED = {
  id: "qemkSAjav98bUhCDQOUH74qdhFVPeTsKLM2stfxG-eg",
  title: "订单支付成功通知",
  content: [
    "订单编号 {{keyword1.DATA }}",
    "订单金额 { { keyword2.DATA } }",
    "订单时间 { { keyword3.DATA } }",
    "商家名称 { { keyword4.DATA } }",
    "物品名称 { { keyword6.DATA } }",
    "数量 { { keyword6.DATA } }",
  ]
};
const TMSG_BONUS = {
  id: "w_H4rEssdBxkCK1vNBP72URLw6eZOgUvCH-WeLdBxF4",
  title: "账户资金变动提醒",
  content: [
    "变动金额 {{keyword1.DATA }}",
    "变动原因 { { keyword2.DATA } }",
    "变动时间 { { keyword3.DATA } }",
    "变动备注 { { keyword4.DATA } }",
  ]
};
module.exports = {
  APP_VERSION: APP_VERSION,
  APP_SHARE_TITLE: APP_SHARE_TITLE,
  APPID: APPID,
  PAY_APPID: PAY_APPID,
  PAY_KEY: PAY_KEY,
  MIN_WITHDRAW_AMOUNT: MIN_WITHDRAW_AMOUNT,
  MAX_WITHDRAW_AMOUNT: MAX_WITHDRAW_AMOUNT,
  TMSG_EXCHANGE: TMSG_EXCHANGE,
  TMSG_ORDER_PAYED: TMSG_ORDER_PAYED,
  TMSG_BONUS: TMSG_BONUS,
  USE_UNIONID: USE_UNIONID,
  DEF_AVATAR: DEF_AVATAR,
  DEF_AVATAR_ACTIVE: DEF_AVATAR_ACTIVE,
  FOR_FIRST_AUDIT: FOR_FIRST_AUDIT,
  REFRESH_INTERVAL: REFRESH_INTERVAL,
  API_URI: API_URI,
  API_URI_TEST: API_URI_TEST,
}