const app = getApp();
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');
const config = require('../../classes/application/config.js');
Page({
  data: {
    themes : null,
    options : null,
    progressPercent:0,
    progressTimer : null,
  },
  onLoad: function (options) {
    console.log("navigate:onLoad:options="+JSON.stringify(options));
    this.setData({
      themes:app.globalData.themes,
      options: options
    });
  },
  onShow: function () {
    this.progress();
  },
  progress : function(){
    var p = this.data.progressPercent + my.random(1, 1 + this.data.progressPercent/3);
    if(p>=100){
      this.setData({
        progressPercent: 100,
      });
      this.loadData();
    }else{
      this.setData({
        progressPercent: p,
      });
      setTimeout(this.progress,100);
    }
  },
  loadData : function(){
    var that = this;
    user.details({
      uid: app.globalData.game.uid,
      callback: function (data) {
        console.warn("------------------ navigate ----------------------------")
        console.warn("navigate:old:user balance=" + app.globalData.userInfo.balance);
        console.warn("navigate:old:game=" + JSON.stringify(app.globalData.game));
        console.warn("navigate:old:cfg.game=" + JSON.stringify(app.globalData.cfg.game));
        app.globalData.userInfo = data;
        app.updateGame(data.game);
        console.warn("navigate:loadData:game=" + JSON.stringify(data.game));
        console.warn("navigate:new:user balance=" + app.globalData.userInfo.balance);
        console.warn("navigate:new:game=" + JSON.stringify(app.globalData.game));
        console.warn("navigate:new:cfg.game=" + JSON.stringify(app.globalData.cfg.game));
        console.warn("-------------------------------------------------------");
        wx.reLaunch({
          url: my.appendOptions(game.homePath(data.game), that.data.options)
        });
      }
    });
    
  },
})
