// pages/error/error.js
const app = getApp();
const my = require('../../classes/lib/util.js');
Page({
  data: {
    error : null,
    ifShowDetails : 0,
    hasReferer : 0,
  },
  onLoad: function (options) {
    var data = my.L("error_message");
    var currentPages = getCurrentPages();
    console.log("currentPages=" + JSON.stringify(currentPages));
    var hasReferer = (data.mode == 0 && currentPages.length > 2 )?1:0;
    console.log("hasReferer=" + hasReferer);
    this.setData({
      error: data,
      hasReferer: hasReferer
    });
  },
  showDetails : function(){
    this.setData({
      ifShowDetails : 1
    });
  },
  doReturn : function(){
    wx.navigateBack({
      delta : 2
    });
  },
  onUnload : function(){
    console.log("error.error onUnload");
    console.log("mode="+this.data.error.mode);
    if(this.data.error.mode==1 && this.data.hasReferer==0){
      wx.switchTab({
        url: '../activity/index',
      });
    }else{
      wx.navigateBack({
        delta: 2
      });
    }
  },
})