const app = getApp();
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');
Page({

  data: {
    themes : null,
    gameSetting:null,
    game : null,
    tabbarMode : 0,
    user:null,
  },

  onLoad: function (options) {
    this.setData({
      themes: app.globalData.themes,
      user:app.globalData.userInfo,
      gameSetting: app.globalData.cfg.game,
      game: game.parse(app.globalData.game),
      tabbarMode : (app.globalData.game.level>1)?1:0,
    });
  },
  loadUserCount : function(){
    var that = this;
    user.user_all({
      openid: app.globalData.openid,
      page :0,
      pageSize:2,
      callback: function (data) {
        console.log("start:loadUserCount="+data.data.length);
        var tabbarMode = that.data.tabbarMode;
        if (data.data.length > 1){
          tabbarMode = 1;
        }
        that.setData({
          tabbarMode: tabbarMode,
        });
      }
    });    
  },
  doNext : function(){
    wx.navigateTo({
      url: '../pay/pay',
    });
  },
  onShareAppMessage: app.share,

})