const app = getApp();
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');
Page({

  data: {
    themes: null,
    loadTimer: null,
    loadStop: false,
    user: null,
    game: null,
    balanceText: [],
    lastAmount: 0,
    castTimeText: 0,
  },

  onLoad: function (options) {
    this.setData({
      themes: app.globalData.themes,
    });
  },
  onShow: function () {
    this.setData({
      loadStop: false
    });
    this.loadUser();
  },
  stopTimer: function () {
    if (this.data.loadTimer) {
      clearTimeout(this.data.loadTimer);
      this.setData({
        loadTimer: null,
        loadStop: true,
      })
    }
  },
  onHide: function () {
    console.log("speedup:onHide:invoke");
    this.stopTimer();
  },
  onUnload: function () {
    console.log("speedup:onUnload:invoke");
    this.stopTimer();
  },

  loadUser: function () {
    var that = this;
    user.details({
      uid: app.globalData.game.uid,
      callback: function (data) {
        that.setData({
          user: data,
          game: game.parse(data.game),
        });
        if (!that.data.loadStop) {
          that.setData({
            loadTimer: setTimeout(that.loadUser, 1000),
          });
        }
      }
    });
  },
})