const app = getApp();
const my = require('../../classes/lib/util.js');
const consts = require('../../classes/consts.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');
const application = require('../../classes/application/application.js');
Page({
  data: {
    themes: null,
    referralsList :null,
    game :null,
    tabbar4: "active",
    gameSetting:null,
    user : null,
  },
  onLoad: function (options) {
    this.setData({
      themes: app.globalData.themes,
      user:app.globalData.userInfo,
      game:app.globalData.game,
      gameSetting: app.globalData.cfg.game,
    });
    this.loadReferrals();
  },
  onShow : function(){
    application.speedBarClose(this);
  },
  onHide: function () {
    console.log("share:onHide:invoke");
    game.refreshSpeedStop(this);
  },
  onUnload: function () {
    console.log("share:onUnload:invoke");
    game.refreshSpeedStop(this);
  },
  onPullDownRefresh: function () {
    this.loadReferrals();
  },
  openSpeedbar: function () {
    if (this.data.speedBarStatus == 0) {
      application.speedBarOpen(this);
      game.refreshSpeedStart(this);
    } else {
      application.speedBarClose(this);
      game.refreshSpeedStop(this);
    }
  }, 
  loadReferrals : function(){
    var that = this;
    user.user_referrals({
      uid : app.globalData.game.uid,
      page : 0,
      pageSize: that.data.gameSetting.referrals_condition+1,
      callback : function(data){
        wx.stopPullDownRefresh();  
        if (data.data.length >= that.data.gameSetting.referrals_condition) {
          wx.reLaunch({
            url: '../shareMore/shareMore',
          });
          return;
        }
        that.setData({
          referralsList: game.share3usersList(data.data),
        });
        //setTimeout(that.loadReferrals, consts.REFRESH_INTERVAL);
      }
    })
  },
  doNext: function () {
    wx.navigateTo({
      url: '../sharePop/sharePop',
    })
  },
  onShareAppMessage: app.share,

})