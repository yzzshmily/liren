const app = getApp();
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
const address = require('../../classes/application/address.js');
Page({

  data: {
    themes: null,
    user: null,
    formValues: null,
    verify_code: null,
    sendVerifyCodeText:'发送验证码',
    nextTimes : 0,
    mobile : '',
  },
  onLoad: function (options) {
    this.setData({
      themes: app.globalData.themes,
      user: app.globalData.userInfo,
    })
  },
  freshTime : function(){
    console.log("password:freshTime:nextTimes=" + this.data.nextTimes);
    if (this.data.nextTimes==0){
      this.setData({
        sendVerifyCodeText : '发送验证码',
      });
    }else{
      this.setData({
        sendVerifyCodeText : '(' + this.data.nextTimes+'S)',
        nextTimes: this.data.nextTimes-1
      });
      setTimeout(this.freshTime,1000);
    }
  },
  mobileInput:function(e){
    this.setData({
      mobile : e.detail.value
    });
  },
  formSubmit: function (e) {
    var data = e.detail.value;
    this.setData({
      formValues: data
    });
    if(this.data.mobile==''){
      my.toast("请输入手机号码");
      return;
    } else if (data.vierify_code == '') {
      my.toast("请输入验证码");
      return;
    } else if (data.vierify_code != this.data.verify_code) {
      my.toast("验证码错误");
      return;
    }else if(data.password==''){
      my.toast("请输入密码");
      return;
    } else if (data.password != data.password2) {
      my.toast("两次输入的密码不一致");
      return;
    }
    console.log(JSON.stringify(this.data.formValues));
    this.updatePassword();
  },
  sendVerifyCode : function(e){
    console.log("password:sendVerifyCode:invoke");
    if (!this.data.mobile || this.data.mobile == '') {
      my.toast("请输入手机号码");
      return;
    }
    this.setData({
      nextTimes:120,
      verify_code : my.radom(1001,9999),
    });
    this.freshTime();
    application.send_verfiy_code({
      uid : app.globalData.game.uid,
      openid : app.globalData.openid,
      mobile : this.data.mobile,
      code : this.data.verify_code,
      callback : function(data){

      }
    })
  },

  updatePassword: function (e) {
   user.bind({
     uid: app.globalData.game.uid,
     openid: app.globalData.openid,
     mobile: this.data.mobile,
     password: this.data.formValues.password,
     callback: function (data) {
        wx.navigateBack({
          delta:-1,
        });
     }
   });
  },
})