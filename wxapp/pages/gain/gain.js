const app = getApp();
const consts = require('../../classes/consts.js');
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');
Page({

  data: {
    themes: null,
    gameSetting: null,
    loadTimer: null,
    loadStop: false,
    user: null,
    game: null,
    balanceText: [],
    lastAmount: 0,
    castTimeText: 0,
    tabbar3: "active",
    speedBarOpenStayle: 'my-speedbar-close',
    speedBarStatus: 0,
    speedlog: null,
    speedLogStop: false,
  },

  onLoad: function (options) {
    this.setData({
      themes: app.globalData.themes,
      gameSetting: app.globalData.cfg.game,
    });
  },
  onShow: function () {
    this.setData({
      loadStop: false
    });
    this.loadUser();
  },
  stopTimer: function () {
    if (this.data.loadTimer) {
      clearTimeout(this.data.loadTimer);
      this.setData({
        loadTimer: null,
        loadStop: true,
      })
    }
  },
  stopSpeedLog: function () {
    this.setData({
      speedLogStop: true,
    })
  },
  onHide: function () {
    console.log("speedup:onHide:invoke");
    this.stopTimer();
    this.stopSpeedLog();
  },
  onUnload: function () {
    console.log("speedup:onUnload:invoke");
    this.stopTimer();
    this.stopSpeedLog();
  },
  loadSpeedLogs: function (first) {
    var that = this;
    game.game_shift_history({
      uid: 0,
      loading: false,
      page: 0,
      pageSize: 1,
      callback: function (data) {
        var speedlog = (data.data && data.data.length > 0) ? data.data[0] : null;
        if (speedlog) {
          speedlog = "Edwon 加速了 " + speedlog.serial + " 名";
        } else {
          speedlog = "没有加速记录";
        }
        that.setData({
          speedlog: speedlog
        });
        if (!first && !that.data.speedLogStop) {
          setTimeout(that.loadSpeedLogs, consts.REFRESH_INTERVAL);
        }
      }
    });
  },
  openSpeedbar: function () {
    console.log("speedup:openSpeedbar:status=" + this.data.speedBarStatus);
    if (this.data.speedBarStatus == 0) {
      this.setData({
        speedBarStatus: 1,
        speedBarOpenStayle: "my-speedbar-open"
      });
      this.loadSpeedLogs();
    } else {
      this.stopTimer();
      this.setData({
        speedBarStatus: 0,
        speedBarOpenStayle: "my-speedbar-close"
      });
    }
    console.log("speedup:openSpeedbar:status=" + this.data.speedBarStatus);
    console.log("speedup:openSpeedbar:speedBarOpenStayle=" + this.data.speedBarOpenStayle);
  },
  loadUser: function () {
    var that = this;
    user.details({
      uid: app.globalData.game.uid,
      callback: function (data) {
        if (data.game.level != app.globalData.game.level) {
          console.log("speedup:loadUser:goto next level");
          that.stopTimer();
          my.alert("恭喜您晋级！请重新登录进入下一关！");
          app.globalData.step = 0;
          wx.redirectTo({
            url: '../index/index',
          });
        }
        that.setData({
          user: data,
          game: game.parse(data.game),
        });
        if (!that.data.loadStop) {
          console.log("speedup:loadUser:refresh_intverval=" + consts.REFRESH_INTERVAL);
          that.setData({
            loadTimer: setTimeout(that.loadUser, consts.REFRESH_INTERVAL),
          });
        }
      }
    });
  },
  doNext: function () {
    wx.navigateTo({
      url: '../sharePop/sharePop',
    })
  }
})