const app = getApp();
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
const news = require('../../classes/application/news.js');
const WxParse = require('../../wxParse/wxParse.js');

Page({

  data: {
    themes: null,
    news : null,
    content : '',
  },

  onLoad: function (options) {
    this.setData({
      themes: app.globalData.themes,
    });
    this.loadNews(options.id);
  },
  loadNews : function(id){
    var that = this;
    news.details({
      id : id,
      callback : function(data){
        that.setData({
          news : data
        });
        WxParse.wxParse('content', 'html', data.content, that, 5);        
      }
    })
  },
  onShareAppMessage: app.share,

})