const app = getApp();
const my = require('../../classes/lib/util.js');
const WxParse = require('../../wxParse/wxParse.js');
const user = require('../../classes/application/user.js');
const goods = require('../../classes/application/goods.js');
const order = require('../../classes/application/order.js');
const wxpay = require('../../classes/application/wxpay.js');
const address = require('../../classes/application/address.js');
Page({
  data: {
    themes: null,
    goods: null,
    goods_content : '',
  },
  onLoad: function (options) {
    var goods = my.L("goods_details")
    this.setData({
      themes : app.globalData.themes,
      goods: goods,
    });
    var content = goods.goods_description;
    content = content.replace(/\t/gi,'');
    content = content.replace(/\r/gi, '');
    content = content.replace(/\n/gi, '');
    WxParse.wxParse('goods_content', 'html', content, this, 5);
  },
  doPay: function () {
    var that = this;
    order.create_first({
      uid: app.globalData.game.uid,
      address_id: 0,
      amount: 1,
      callback: function (data) {
        that.setData({
          order: data
        });
        if (data.pay_status > 1) {
          wx.reLaunch({
            url: '../shipping/shipping?order_id=' + data.order_id,
          });
        } else {
          that.doWxpay();
        }
      },
    })
  },
  doWxpay: function () {
    var that = this;
    wxpay.call({
      page: this,
      openid: app.globalData.openid,
      order_id: this.data.order.order_id,
      order_type: my.val(this.data.order.order_type),
      success: function (data) {
        that.setData({
          paySucceed: 1
        });
        wx.reLaunch({
          url: '../shipping/shipping?order_id=' + that.data.order.order_id,
        });
      },
      error: function (res) {
        console.log(JSON.stringify(res));
        if (res.errMsg != 'requestPayment:fail cancel') {
          my.alert("fail:" + JSON.stringify(res));
        }
      },
    });
  },
  onShareAppMessage: app.share,

})