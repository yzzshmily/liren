const app = getApp();
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');
Page({

  data: {
    user:null,
    game:null,
    themes: null,
    referralsList : [],
  },
  onPullDownRefresh: function () {
    this.loadShareList();
  },
  onLoad: function (options) {
    this.setData({
      user:app.globalData.userInfo,
      game:app.globalData.game,
      themes: app.globalData.themes,
    });
    this.loadShareList();
  },

  loadShareList : function(){
    var that = this;
    user.user_referrals({
      uid : app.globalData.game.uid,
      page : 0,
      pageSize : 300,
      callback : function(data){
        wx.stopPullDownRefresh();
        that.setData({
          referralsList : game.parseUserList(data.data)
        })
      }
    })
  },
  onShareAppMessage: app.share,

})