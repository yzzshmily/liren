const app = getApp();
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
const news = require('../../classes/application/news.js');
Page({

  data: {
    themes: null,
    newsList : null,
    tabbar1 : 'active',
  },
  onLoad: function (options) {
    this.setData({
      themes: app.globalData.themes,
    });
    this.loadNewsList();
  },
  loadNewsList : function(){
    var that = this;
    news.list({
      page : 0,
      pageSize : 50,
      callback : function(data){
        var list = [],di=null;
        for(var i=0;i<data.data.length;i++){
          di = data.data[i];
          di.desc = (!di.desc || di.desc=='')?di.title:di.desc;
          di.date = my.formatDate();
          list.push(di);
        }
        that.setData({
          newsList : list
        })
      }
    })
  },
  onShareAppMessage: app.share,

})