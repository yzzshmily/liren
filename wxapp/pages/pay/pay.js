const app = getApp();
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
const goods = require('../../classes/application/goods.js');
const order = require('../../classes/application/order.js');
const wxpay = require('../../classes/application/wxpay.js');
const address = require('../../classes/application/address.js');
Page({

  data: {
    themes: null,
    goods : null,
    address :null,
    order : null,
    gameSetting:null,
    orderExists : false,
  },
  onLoad: function (options) {
    var orderExists = app.orderExist(app.globalData.game.uid,app.globalData.game.level);
    this.setData({
      themes: app.globalData.themes,
      gameSetting: app.globalData.cfg.game,
      orderExists: orderExists
    })
    this.loadGoods();
  },
  loadGoods : function(){
    var that = this;
    goods.details({
      gid: app.globalData.game.goods,
      callback : function(data){
        that.setData({
          goods : data
        });
      },
    })
  },
  doPay: function () {
    var that = this;
    order.create_first({
      uid: app.globalData.game.uid,
      address_id: 0,
      amount: 1,
      callback: function (data) {
        that.setData({
          order: data
        });
        if (data.pay_status > 1) {
          app.orderPush();
          wx.redirectTo({
            url: '../shipping/shipping?order_id='+data.order_id,
          });
        } else {
          that.doWxpay();
        }
      },
    })
  },
  doWxpay: function () {
    var that = this;
    wxpay.call({
      page: this,
      openid: app.globalData.openid,
      order_id: this.data.order.order_id,
      order_type: my.val(this.data.order.order_type),
      success: function (data) {
        that.setData({
          paySucceed: 1
        });
        app.orderPush();
        wx.redirectTo({
          url: '../shipping/shipping?order_id='+that.data.order.order_id,
        });
      },
      error: function (res) {
        console.log(JSON.stringify(res));
        if (res.errMsg != 'requestPayment:fail cancel') {
          my.alert("fail:" + JSON.stringify(res));
        }
      },
    });
  },
  showGoodsDetails : function(){
    my.L("goods_details",this.data.goods);
    wx.navigateTo({
      url: '../goods/goods',
    });
  },
  onShareAppMessage: app.share,

})