const app = getApp();
const my = require('../../classes/lib/util.js');
const pager = require('../../classes/lib/pager.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');

Page({

  data: {
    themes: null,
    userList: null,
    currentUid: 0,
    gameSetting: null,
    hasNext: false,
  },

  onLoad: function (options) {
    pager.init();
    this.setData({
      themes: app.globalData.themes,
      currentUid: app.globalData.game.uid,
      gameSetting: app.globalData.cfg.game,
    });
    this.loadUserAll();
  },
  onPagerNext: function () {
    this.loadUserAll();
  },
  loadUserAll: function () {
    var that = this;
    user.user_all({
      openid: app.globalData.openid,
      page: pager.next(),
      pageSize: pager.pageSize(),
      callback: function (data) {
        var list = game.parseUserList(data.data, app.globalData.game.uid);
        console.log("userSwitch:loadUserAll:size=" + data.data.length);
        that.setData({
          hasNext: pager.hasNext(data.data.length),
          userList: my.concat(that.data.userList, list),
        });
      }
    });
  },

  doSwitch: function (e) {
    console.log("userSwitch:doSwitch:e=" + JSON.stringify(e));
    var uid = my.ival(e.currentTarget.dataset.uid);
    if (uid <= 0) {
      my.toast("错误：无效的UID");
      return;
    }
    my.L("user_data_last_uid", uid);
    my.L("selected_region", null);
    app.globalData.step = 0;
    wx.reLaunch({
      url: '../index/index',
    });
  }
})