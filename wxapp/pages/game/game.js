const app = getApp();
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
Page({

  data: {
    themes: app.globalData.themes,
    tabbar2:"active"
  },
  onLoad: function (options) {
    this.setData({
      themes: app.globalData.themes
    })
  },


  doNext: function () {
    wx.navigateTo({
      url: '../sharePop/sharePop',
    })
  }
})