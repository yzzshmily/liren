const app = getApp();
Page({

  data: {
    oid : null,
    uid : null,
  },

  onLoad: function (options) {
    this.setData({
      oid : app.globalData.openid,
      uid : app.globalData.game.uid,
    })  
  },
  getWapMessage:function(e){
    console.log("payWap:getWapMessage:data="+JSON.stringify(e.detail));
  }
})