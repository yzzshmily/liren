const app = getApp();
const consts = require('../../classes/consts.js');
const my = require('../../classes/lib/util.js');
const pager = require('../../classes/lib/pager.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');
const application = require('../../classes/application/application.js');
Page({

  data: {
    themes: null,
    gameSetting:null,
    user : null,
    game : null,
    tabbar4:"active",  
  },

  onLoad: function (options) {
    pager.init();
    this.setData({
      themes: app.globalData.themes,
      user: app.globalData.userInfo,
      game:app.globalData.game,
      gameSetting:app.globalData.cfg.game,  
    });
  },
  onShow : function(){
    application.speedBarClose(this);
    user.refreshStart(this);
    game.refreshCostTime(this);
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },
  onHide: function () {
    console.log("speedup:onHide:invoke");
    user.refreshStop(this);
    game.refreshSpeedStop(this);
  },
  onUnload: function () {
    console.log("speedup:onUnload:invoke");
    user.refreshStop(this);
    game.refreshSpeedStop(this);
  },
  openSpeedbar: function () {
    if (this.data.speedBarStatus == 0) {
      application.speedBarOpen(this);
      game.refreshSpeedStart(this);
    } else {
      application.speedBarClose(this);
      game.refreshSpeedStop(this);
    }
  },  

  doNext: function () {
    wx.navigateTo({
      url: '../sharePop/sharePop',
    })
  },
  onShareAppMessage: app.share,

})