const app = getApp();
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
Page({
  data: {
    gender: 1,
    noteList: [],
    radioData: [
      { name: '1', value: '我是帅哥', checked: 'true' },
      { name: '2', value: '我是美女' },
    ],
    signinFlow: { "finished": 0, "gender": 1 }
  },

  onLoad: function (options) {
    this.setData({
      windowHeight: app.globalData.systemInfo.windowHeight,
    });
  },
  onShow: function () {

  },
  radioChange: function (e) {
    this.setData({
      gender: e.detail.value
    });
    app.setGender(e.detail.value);
    this.data.signinFlow.gender = e.detail.value;
  },
  doNext: function (e) {
    var that = this;
    user.role_gender({
      uid: app.globalData.game.uid,
      sex: this.data.gender,
      callback: function (data) {
        app.setGender(that.data.gender);
        wx.navigateTo({
          url: '../h24start/h24start',
        })
      },
      error: function (code, msg) {
        alert(msg);
      }
    })
  }
})