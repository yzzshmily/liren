const app = getApp();
const my = require('../../classes/lib/util.js');
const consts = require('../../classes/consts.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');
const application = require('../../classes/application/application.js');
Page({

  data: {
    themes: null,
    gameSetting:null,
    referralsList:null,
    user : null,
    game : null,
    tabbar4:"active",
  },

  onLoad: function (options) {
    this.setData({
      themes: app.globalData.themes,
      gameSetting:app.globalData.cfg.game,
      user:app.globalData.userInfo,
      game: app.globalData.game,
    });
    this.loadReferrals();
  },
  onShow : function(){
    application.speedBarClose(this);
    user.refreshStart(this);
  },

  onHide: function () {
    console.log("shareMore:onHide:invoke");
    game.refreshSpeedStop(this);
    user.refreshStop(this);
  },
  onUnload: function () {
    console.log("shareMore:onUnload:invoke");
    game.refreshSpeedStop(this);
    user.refreshStop(this);
  },
  openSpeedbar: function () {
    if (this.data.speedBarStatus == 0) {
      application.speedBarOpen(this);
      game.refreshSpeedStart(this);
    } else {
      application.speedBarClose(this);
      game.refreshSpeedStop(this);
    }
  }, 
  onPullDownRefresh: function () {
    user.check();
    this.loadReferrals();
  },
  loadReferrals: function () {
    var that = this;
    user.user_referrals({
      uid: app.globalData.game.uid,
      page: 0,
      pageSize: that.data.gameSetting.referrals_condition + 1,
      callback: function (data) {
        that.setData({
          referralsList: game.share3usersList(data.data,that.data.game.referrals),
        });
      }
    })
  },
  doNext: function () {
    wx.navigateTo({
      url: '../sharePop/sharePop',
    })
  },
  onShareAppMessage: app.share,

})