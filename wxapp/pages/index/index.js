const app = getApp();
const consts = require('../../classes/consts.js');
const my = require('../../classes/lib/util.js');
const demo = require('../../classes/application/_demo.js');
const user = require('../../classes/application/user.js');
const chulan = require('../../classes/application/chulan.js');
const game = require('../../classes/application/game.js');
const config = require('../../classes/application/config.js');
const application = require('../../classes/application/application.js');
const wechat = require('../../classes/application/wechat.js');
Page({
  data: {
    options:null,
    windowHeight : 0,
    loadingOK : 0,
    out : 0,
    loadingProcess : 0,
    path : '',
    chulan_passport : 0,
    chulan_passport_pass : 0,
    system_offline: 0,
    progressPercent:0,
    version:"",
    user:null,
    debug : {
      open : false,
      page : "",
      gender : 1,
      task : 0,
      nologin : false,
      level : 1,
      h24:0,
      off:0,
      chulan:0,
      task:0,
    },
    app_scene : null,
    user_mobile : null,
  },
  onLoad: function (options) {
    console.log("index:onLoad:options="+JSON.stringify(options));
    var _options = (app.globalData.index_options)?app.globalData.index_options:options;
    this.setData({
      options: _options,
      debug : {
        open: (options.debug == 1),
        page: (options.page)?"../"+options.page+"/"+options.page:null,
        gender: (options.gender)?options.gender:null,
        level: (options.level) ? options.level : null,
        nologin:(options.nologin==1),
        h24:(options.h24==1)?1:0,
        off:(options.off==1)?1:0,
        chulan:(options.chulan==1)?1:0,
        task: (options.task) ? my.ival(options.task) : null,
      },
      windowHeight: app.globalData.systemInfo.windowHeight,
      version: "V"+consts.APP_VERSION,
    });
    app.globalData.from_user_id = my.ival(_options.rid);
  },
  onShow : function(){
    if (this.data.debug.open && this.data.debug.nologin) {
      app.globalData = demo.globalData;
    }else{
    }
    if (app.globalData.step < 4) {
      app.step0_wxLogin();
      app.startLoginTimer(this.preparData);
    } else {
      this.preparData();
    }
  },
  preparData : function(){
    if (app.globalData.step < 4){
      this.process();
      return;
    }
    console.log("index:preparData:step="+app.globalData.step);
    app.clearLoginTimer();    
    this.loadOK();
  },
  process: function(){
    var max = Math.ceil(100 * ((app.globalData.step+1) / 4));
    var min = this.data.progressPercent;
    var precent = my.random(min,max);;
    this.setData({
      progressPercent: precent
    })
  },
  loadOK : function(){
    this.setData({
      loadingOK: 1,
      user: app.globalData.userInfo,
      user_mobile: app.globalData.userInfo.mobile,
      app_scene : app.globalData.scene,
    });
    console.log("index:loadOK");

    if (this.data.debug.open) {
      console.log("index:onShow:debug:open");
      /*调试信息*/
      if (this.data.debug.level) {
        app.globalData.game.level = this.data.debug.level;
      }
      if (this.data.debug.task){
        app.globalData.game.task = this.data.debug.task;
      }
      if (this.data.debug.gender) {
        console.log("index:onShow:debug:gender=" + this.data.debug.gender);
        app.setGender(this.data.debug.gender);
      }
      console.log("index:onShow:debug:h24=" + this.data.debug.h24);
      app.globalData.cfg.app.opening_24_hour = this.data.debug.h24;

      console.log("index:onShow:debug:chulan_passport=" + this.data.debug.chulan);
      app.globalData.cfg.app.chulan_passport = this.data.debug.chulan;

      console.log("index:onShow:debug:off=" + this.data.debug.off);
      app.globalData.cfg.app.system_offline = this.data.debug.off;

      if(this.data.debug.page){
        console.log("index:onShow:debug:goto="+this.data.debug.page);
        wx.reLaunch({
          url: this.data.debug.page,
        });
        return;
      }
    }else{
      console.log("index:onShow:debug:close");
    }

    if (app.globalData.cfg.app.system_offline==1){
      this.setData({
        system_offline: 1
      });
      return;
    }

    /*********************************
    task 0 新用户（没买东西） ；
    task 1 买过东西（未完成推荐任务），
    task 2 在排队队列
    task 3 正在拿钱
    task 4 出局（通关）
    **********************************/
    var path = '';
    if (app.globalData.game.task == 4 && app.globalData.game.balance == 0){
      this.setData({
        out: 1
      });
      return;
    }
    path = game.homePath(app.globalData.game);
    my.L("login_path", path);
    console.log("index:loadOK:path="+path);    
    this.setData({
      path : path,
      chulan_passport_pass: (app.globalData.cfg.app.chulan_passport==1)?0:1,
      chulan_passport: app.globalData.cfg.app.chulan_passport,
    });
    console.log("index:loadOK:chulan_passport=" + this.data.chulan_passport);  
    console.log("index:loadOK:chulan_passport_pass=" + this.data.chulan_passport_pass);  
    console.log("index:loadOK:loadingOK=" + this.data.loadingOK);  
    console.log("index:loadOK:user_mobile=" + this.data.user_mobile);  
    console.log("index:loadOK:task=" + app.globalData.game.task);  
    console.log("index:loadOK:out=" + this.data.out);  
  },
  chulanCheckSubmit:function(e){
    this.setData({
      formValues: e.detail.value
    });
    console.log("index:chulanCheckSubmit:"+JSON.stringify(this.data.formValues));
    var username = e.detail.value.username;
    if(username==''){
      my.toast("请输入帐号");
      return;
    }
    if(username=="sandu"){
      this.chulanLoginOK(username);
      return;
    }
    var that = this;
    chulan.passport({
      username : username,
      password : '',
      callback : function(data){
        that.chulanLoginOK(username);
      },
      error : function(code,msg){
        my.alert(msg);
      }
    });
  },
  chulanLoginOK : function(username){
    my.toast("登录成功");
    app.globalData.chulan_username = username;
    this.setData({
      chulan_passport_pass : 1,
    });
  }, 
  doEnterWithGetPhone:function(e){
    console.log("index:doEnterWithGetPhone:errMsg:" + e.detail.errMsg);
    console.log("index:doEnterWithGetPhone:iv:" + e.detail.iv);
    console.log("index:doEnterWithGetPhone:encryptedData:" + e.detail.encryptedData);
    if(e && e.detail.iv){
      var info = wechat.bizDataCrypt(
        app.globalData.session_key,
        e.detail.encryptedData,
        e.detail.iv
      );
      console.log("index:doEnterWithGetPhone:info:" + JSON.stringify(info));
      var _mobile = info.purePhoneNumber;
      if (_mobile) {
        app.globalData.userInfo.mobile = _mobile;
        my.L("get_phone_number", _mobile);
        user.bind({
          uid: app.globalData.game.uid,
          openid: app.globalData.openid,
          mobile: _mobile,
          password: my.random(100001,999999),
          callback: function (data) {
            wx.navigateBack({
              delta: -1,
            });
          }
        });
      }else{
        console.log("index:doEnterWithGetPhone:purePhoneNumber is empty");
      }
    }else{
      console.log("index:doEnterWithGetPhone:user deny for getPhoneNumber");
    }
    this.doEnter();
  },
  doEnter : function(e){
    console.log("index:doEnter:path="+this.data.path);
    wx.reLaunch({
      url: this.data.path,
    });
  },
  checkSystemSetting : function(){
    application.init({
      params: app.globalData.loginParams,
      callback: function (data) {
        console.log("index:checkSystemSetting:data="+JSON.stringify(data.setting));
        if(data.setting.system_offline==1){
          my.alert("系统尚未开启，请稍后再来！");
        }else{
          application.relogin();
        }
      }    
    });
  },
  onShareAppMessage:app.share,
})
