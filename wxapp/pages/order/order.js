const app = getApp();
const my = require('../../classes/lib/util.js');
const pager = require('../../classes/lib/pager.js');
const user = require('../../classes/application/user.js');
const order = require('../../classes/application/order.js');
const demo = require('../../classes/application/_demo.js');
Page({

  data: {
    themes: null,
    orderList : null,
    hasNext: "加载更多",
  },

  onLoad: function (options) {
    this.setData({
      themes: app.globalData.themes
    })
  },
  onShow : function(){
    this.reloadOrders();  
  },
  onPagerNext:function(){
    this.loadOrders();  
  },
  reloadOrders : function(){
    pager.init();
    this.setData({
      orderList: [],
    });
    this.loadOrders();
  },
  onPullDownRefresh: function () {
    this.reloadOrders();
  },
  loadOrders : function(){
    var that = this;
    order.list({
      uid : app.globalData.game.uid,
      openid : app.globalData.openid,
      page: pager.next(),
      pageSize : pager.pageSize(),
      callback : function(data){
        wx.stopPullDownRefresh();
        var list= order.parseList(data.data);
        that.setData({
          hasNext: pager.hasNext(data.data.length),
          orderList: my.concat(that.data.orderList,list),
        });
      },
    });
  },
  onShareAppMessage: app.share,

})