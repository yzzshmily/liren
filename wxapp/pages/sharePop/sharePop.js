const app = getApp();
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
const order = require('../../classes/application/order.js');
const game = require('../../classes/application/game.js');
const wxpay = require('../../classes/application/wxpay.js');
Page({

  data: {
    themes: null,
    user: null,
    game:null,
    order: null,
    gameSetting:null,
  },

  onLoad: function(options) {
    this.setData({
      themes: app.globalData.themes,
      user: app.globalData.userInfo,
      game:app.globalData.game,
      gameSetting:app.globalData.cfg.game,
    });
    my.L("speed_pay_succeed_ts", null);
  },
  doBack: function() {
    wx.navigateBack({
      delta: -1
    })
  },
  doBuy: function(e) {
    var amount = my.ival(e.currentTarget.dataset.amount);
    var that = this;
    order.create_more({
      uid: app.globalData.game.uid,
      amount: amount,
      address_id: (app.globalData.address) ? app.globalData.address.address_id : 0,
      callback: function(data) {
        console.log("sharePop:doWxpay:create order succeed,order_id="+data.order_id);
        that.setData({
          order: data
        });
        if (data.pay_status >= 2) {
          app.orderPush();
          console.log("sharePop:balance pay:succeed, order_id=" + that.data.order.order_id);
          my.L("speed_pay_succeed_ts", (new Date()).getTime());
          user.navigate(that,function(game){
            app.updateGame(game);
          });
        } else {
          that.doWxpay();
        }
      }
    })
  },
  doWxpay: function() {
    var that = this;
    wxpay.call({
      page: this,
      openid: app.globalData.openid,
      order_id: this.data.order.order_id,
      order_type: my.val(this.data.order.order_type),
      success: function(data) {
        app.orderPush();
        console.log("sharePop:doWxpay:succeed, order_id="+that.data.order.order_id);
        my.L("speed_pay_succeed_ts", (new Date()).getTime());
        user.navigate(that, function(game) {
          app.updateGame(game);
        });
      },
      error: function(res) {
        console.log(JSON.stringify(res));
        if (res.errMsg != 'requestPayment:fail cancel') {
          my.alert("fail:" + JSON.stringify(res));
        }
      },
    });
  },
  onShareAppMessage: app.share,

})