const app = getApp();
const my = require('../../classes/lib/util.js');
const consts = require('../../classes/consts.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');
const application = require('../../classes/application/application.js');
Page({

  data: {
    themes: null,
    gameSetting: null,
    referralsList: null,
    user: null,
    game: null,
    tabbar2: "active",
    tabbarMode:2,
    btnProgress : 0,
  },

  onLoad: function (options) {
    this.setData({
      themes: app.globalData.themes,
      gameSetting: app.globalData.cfg.game,
      user: app.globalData.userInfo,
      game: app.globalData.game,
    });
    this.loadReferrals();
  },
  onShow: function () {
    application.speedBarClose(this);
    user.refreshStart(this);
    var that = this;
    var restTs = 60000 - (new Date()).getTime() + my.ival(my.L("speed_pay_succeed_ts"));
    console.warn("h24waiting:onShow:restTs="+restTs);
    my.L("speed_pay_succeed", null);
    if (restTs>0){
      restTs = my.ival(restTs/2);
      this.setData({
        btnProgress : my.ival(((restTs * 100) / 1000) / 60),
      });
      this.refreshBtnProgress();
    }else{
      this.setData({
        btnProgress: 100,
      });
    }
  },
  refreshBtnProgress:function(){
    var btnProgress = this.data.btnProgress + 1.7;
    if (btnProgress >= 100) {
      btnProgress = 100;
    }
    this.setData({
      btnProgress: btnProgress,
    });
    if (btnProgress<100){
      setTimeout(this.refreshBtnProgress,1000);
    }else{
      this.loadReferrals();
    }
  },
  onHide: function () {
    console.log("h24wating:onHide:invoke");
    game.refreshSpeedStop(this);
    user.refreshStop(this);
  },
  onUnload: function () {
    console.log("h24wating:onUnload:invoke");
    game.refreshSpeedStop(this);
    user.refreshStop(this);
  },
  openSpeedbar: function () {
    if (this.data.speedBarStatus == 0) {
      application.speedBarOpen(this);
      game.refreshSpeedStart(this);
    } else {
      application.speedBarClose(this);
      game.refreshSpeedStop(this);
    }
  },
  onPullDownRefresh: function () {
    this.loadReferrals();
  },
  loadReferrals: function () {
    var that = this;
    user.user_referrals({
      uid: app.globalData.game.uid,
      page: 0,
      pageSize: that.data.gameSetting.referrals_condition + 1,
      callback: function (data) {
        that.setData({
          referralsList: game.share3usersList(data.data, that.data.game.referrals),
        });
      }
    })
  },
  doNext: function () {
    wx.navigateTo({
      url: '../sharePop/sharePop',
    })
  }
})