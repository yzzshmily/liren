const app = getApp();
const consts = require('../../classes/consts.js');
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
Page({

  data: {
    themes: null,
    game:null,
    tabbar3:"active",
    user:null,
    firstAudit : false,
  },

  onLoad: function (options) {
    this.setData({
      themes: app.globalData.themes,
      user:app.globalData.userInfo,
      game:app.globalData.game,
      firstAudit: consts.FOR_FIRST_AUDIT,
    })
  },

  doNext: function () {
    wx.navigateTo({
      url: '../sharePop/sharePop',
    })
  },
  gotoBind : function(e){
    if (my.ival(app.globalData.userInfo.mobile) > 0) {
      console.log("index:getPhoneNumber allready bind");
      console.log("index:getPhoneNumber:mobile=" + app.globalData.userInfo.mobile);
    } else {
      var phone = my.ival(my.L("get_phone_number"));
      if (phone > 0) {
        console.log("index:getPhoneNumber:errMsg:" + e.detail.errMsg);
        console.log("index:getPhoneNumber:iv:" + e.detail.iv);
        console.log("index:getPhoneNumber:encryptedData:" + e.detail.encryptedData);
        var info = wechat.bizDataCrypt(
          app.globalData.session_key,
          e.detail.encryptedData,
          e.detail.iv
        );
        console.log("index:getPhoneNumber:info:" + JSON.stringify(info));
        app.globalData.userInfo.mobile = info.purePhoneNumber;
        my.L("get_phone_number", info.purePhoneNumber);
      } else {
        console.log("index:getPhoneNumber in localstorage");
        console.log("index:getPhoneNumber:info:" + JSON.stringify(info));
        app.globalData.userInfo.mobile = phone;
      }
    }
  },
  onShareAppMessage: app.share,


})