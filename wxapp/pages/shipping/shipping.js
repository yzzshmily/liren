const app = getApp();
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
const address = require('../../classes/application/address.js');
const order = require('../../classes/application/order.js');
const wxpay = require('../../classes/application/wxpay.js');
Page({
  data: {
    themes: null,
    avatar: null,
    formValues: null,
    address: null,
    region: null,
    user: null,
    order: null,
    paySucceed: 0,
    btnStatus: 1,
    picker_text: "请选择",
    order_id: 0,
  },

  onLoad: function(options) {
    console.log("shipping:onLoad:options=" + JSON.stringify(options));
    this.setData({
      order_id: my.ival(options.order_id),
      user: app.globalData.userInfo,
      game:app.globalData.game,
      themes: app.globalData.themes,
      avatar: app.globalData.game.avatar,
      address: app.globalData.address,
    });
  },
  onShow: function() {
    var region = my.L("selected_region");
    if (region) {
      console.log("shipping:onShow:selected_region not null");
      this.setData({
        region: region,
        picker_text: [region.province.region_name, region.city.region_name, region.district.region_name].join(','),
      });
      console.log("shipping:onShow:region after selected:" + JSON.stringify(this.data.region));
      my.L("selected_region", null);
    } else if (this.data.address) {
      console.log("shipping:onShow:data.address not null");
      console.log("shipping:onShow:address:" + JSON.stringify(this.data.address));
      this.setData({
        picker_text: [this.data.address.province, this.data.address.city, this.data.address.district].join(','),
        region: {
          province: {
            region_name: this.data.address.province
          },
          city: {
            region_name: this.data.address.province
          },
          district: {
            region_name: this.data.address.district
          },
        }
      });
    } else {
      this.setData({
        picker_text: '请选择',
      });
    }
  },
  formSubmit: function(e) {
    this.setData({
      formValues: e.detail.value
    });
    this.setData({
      btnStatus: 0
    });
    this.saveAddress();
  },
  doSelectRegion: function(e) {
    wx.navigateTo({
      url: '../region/region',
    });
  },
  saveAddress: function() {
    if (!this.data.formValues.consignee) {
      my.toast("请设置收货人");
      return;
    }
    if (!this.data.formValues.tel) {
      my.toast("请设置手机号码");
      return;
    }
    if (!this.data.region) {
      my.toast("请设置所在区域");
      return;
    }
    if (!this.data.formValues.address) {
      my.toast("请设置详细地址");
      return;
    }
    var that = this;
    var address_id = (this.data.address) ? my.ival(this.data.address.address_id) : 0;
    address.save({
      openid: app.globalData.openid,
      order_id: this.data.order_id,
      address_id: address_id,
      country: 1,
      province: this.data.region.province.region_id,
      city: this.data.region.city.region_id,
      district: this.data.region.district.region_id,
      consignee: this.data.formValues.consignee,
      address: this.data.formValues.address,
      zipcode: '000000',
      tel: this.data.formValues.tel,
      callback: function(data) {
        data.province_name = that.data.region.province.region_name;
        data.city_name = that.data.region.city.region_name;
        data.district_name = that.data.region.district.region_name;
        app.globalData.address = data;
        that.setData({
          address: data
        });
        user.navigate(that, function(game) {
          app.updateGame(game);
        });
      }
    })
  },
  onShareAppMessage: app.share,

})