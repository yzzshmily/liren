const app = getApp();
const my = require('../../classes/lib/util.js');
const pager = require('../../classes/lib/pager.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');
Page({

  data: {
    themes: null,
    logs : [],
    log4uid : 0,
    hasNext : "没有更多内容了",
    game:null,
    user : null,
  },

  onLoad: function (options) {
    pager.init();
    this.setData({
      themes: app.globalData.themes,
      user:app.globalData.userInfo,
      game : app.globalData.game,
      log4uid : (options.all && options.all==1)?0:app.globalData.game.uid,
    });

    this.loadSpeedLogs();
  },
  onPullDownRefresh: function () {
    this.loadSpeedLogs();
  },
  onPagerNext: function () {
    this.loadSpeedLogs();
  },
  loadSpeedLogs : function(){
    var that = this;
    game.shift_history({
      openid:app.globalData.openid,
      page : pager.next(),
      pageSize : pager.pageSize(),
      callback : function(data){
        wx.stopPullDownRefresh();
        that.setData({
          hasNext: pager.hasNext(data.data.length),
          logs : my.concat(that.data.logs,data.data),
        });
      }
    });
  },
  doNext: function () {
    wx.navigateTo({
      url: '../sharePop/sharePop',
    })
  },
  onShareAppMessage: app.share,

})