// pages/setting.js
const app = getApp();
const my = require('../../classes/lib/util.js');
Page({

  data: {
    
  },
  onLoad: function (options) {
  },
  onGotUserInfo: function (e) {
    console.log("onGotUserInfo:errMsg=" + e.detail.errMsg);
    console.log("onGotUserInfo:errMsg=" + JSON.stringify(e.detail.userInfo));
    console.log("onGotUserInfo:rawData=" + JSON.stringify(e.detail.rawData));
    app.clearLoginTimer();
    if (e.detail.errMsg == "getUserInfo:ok") {
      wx.reLaunch({
        url: '../index/index',
      });
    }
  }
})