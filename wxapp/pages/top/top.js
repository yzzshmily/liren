const app = getApp();
const my = require('../../classes/lib/util.js');
const pager = require('../../classes/lib/pager.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');
Page({

  data: {
    themes: null,
    tabbar3: "m3-active",
    topList : null,
    loadTimer : null,
    loadStop : 0,
    pageTitle : '',
  },

  onLoad: function (options) {
    pager.init();
    this.setData({
      themes: app.globalData.themes,
    });
    this.loadList();
  },
  onPullDownRefresh: function () {
    this.loadList();
  },
  onPagerNext: function () {
    this.loadList();
  },  
  loadList : function(){
    var that = this;
    if (app.globalData.cfg.game.opening_24_hour==0){
      wx.setNavigationBarTitle({
        title: app.globalData.game.name+'排行榜'
      });
      game.top24({
        page: pager.next(),
        pageSize: pager.pageSize(),
        callback: function (data) {
          wx.stopPullDownRefresh();
          that.setData({
            hasNext:pager.hasNext(data.data.length),
            topList: my.concat(that.data.topList,game.parseUserList(data.data))
          });
        }
      })
    }else{
      wx.setNavigationBarTitle({
        title: '总排行榜'
      });
      game.top({
        uid: app.globalData.game.uid,
        page: 0,
        pageSize: 50,
        callback: function (data) {
          wx.stopPullDownRefresh();
          that.setData({
            topList: game.parseUserList(data.data)
          });
        }
      })
    }
  },
  onShareAppMessage: app.share,

})