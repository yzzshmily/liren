const app = getApp();
const my = require('../../classes/lib/util.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');
const config = require('../../classes/application/config.js');
Page({

  data: {
    themes: null,
    user: null,
    game: null,
    dataLoaded: 0,
    bank: null,
    bank_name:'请选择',
    hasOrders:true,
    amount : 0,
  },

  onLoad: function(options) {
    this.setData({
      themes: app.globalData.themes,
      bank : my.L("user_data_bank"),
    });
    console.log("widhdraw:onLoad:bank="+JSON.stringify(this.data.bank));
    this.loadAcountOrders();
  },
  loadAcountOrders: function () {
    var that = this;
    user.withdrawOrders({
      uid: app.globalData.game.uid,
      status: 0,
      page: 0,
      pageSize: 1,
      callback: function (data) {
        that.setData({
          hasOrders:(data.data && data.data.length>0),
          accountOrders: data.data
        });
        that.loadUser();
      }
    })
  },
  loadUser: function () {
    var that = this;
    user.details({
      uid: app.globalData.game.uid,
      callback: function (data) {
        that.setData({
          user: data,
          amount: data.game.balance,
          game: game.parse(data.game),
        });
        that.loadBanks();
      }
    });
  },
  loadBanks: function() {
    if(app.globalData.cfg.banks){
      return;
    }
    var that = this;
    config.banks({
      callback: function(data) {
        app.globalData.cfg.banks = data;
        that.setData({
          dataLoaded: 1,
        })
      }
    });
  },
  formSubmit: function(e) {
    var re = this.buildUserBankData(e.detail.value);
    if(re){
      this.doWithdraw();
    }
  },
  buildUserBankData : function(formValues){
    if (!this.data.bank) {
      my.toast("请选择开户行");
      return false;
    }
    if (formValues.address == '') {
      my.toast("请输入开户网点");
      return false;
    }
    if (formValues.real_name == '') {
      my.toast("请输入开户姓名");
      return false;
    }
    if (formValues.account == '') {
      my.toast("请输入银行帐号");
      return false;
    }
    if (this.data.bank.mobile == '') {
      my.toast("请输入手机号码");
      return false;
    }
    var bank = this.data.bank;
    bank.address = formValues.address;
    bank.real_name = formValues.real_name;
    bank.account = formValues.account;
    bank.mobile = formValues.mobile;
    bank.note = '';
    this.setData({
      bank : bank
    });
    console.log("widhdraw:buildUserBankData:bank=" + JSON.stringify(bank));
    my.L("user_data_bank",bank);
    return true;
  },
  doWithdraw: function() {
    user.withdraw({
      uid: app.globalData.game.uid,
      amount: app.globalData.game.balance,
      bank_code: this.data.bank.code,
      account: this.data.bank.account,
      real_name: this.data.bank.real_name,
      address: this.data.bank.address,
      note: this.data.bank.note
    })
  },
  doSelectBank: function(e) {
    var idx = e.detail.value;
    console.log("withdraw:doSelectBank:idx=" + idx);
    this.setData({
      bank: app.globalData.cfg.banks[idx],
      bank_name: app.globalData.cfg.banks[idx].name
    });
  },
  onShareAppMessage: app.share,

})