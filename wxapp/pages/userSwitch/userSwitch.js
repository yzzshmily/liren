const app = getApp();
const my = require('../../classes/lib/util.js');
const pager = require('../../classes/lib/pager.js');
const user = require('../../classes/application/user.js');
const game = require('../../classes/application/game.js');
const demo = require('../../classes/application/_demo.js');
Page({

  data: {
    themes: null,
    userList: null,
    currentUid: 0,
    gameSetting: null,
    hasNext: false,
    pagerText : pager.text(),
    pickerLevel : [],
    pickerTask : [],
    search_level : null,
    search_task:null,
    total : 0,
  },

  onLoad: function (options) {
    pager.init(50);
    this.setData({
      themes: app.globalData.themes,
      currentUid: app.globalData.game.uid,
      gameSetting: app.globalData.cfg.game,
      pickerLevel : game.pickerLevel(),
      pickerTask : game.pickerTask(),
      search_level: 0,
      search_task: 0,
    });
    this.loadUserAll();
  },
  onPagerNext: function () {
    this.loadUserAll();
  },
  onPullDownRefresh: function () {
    pager.init(50);
    this.setData({
      userList:[]
    });
    this.loadUserAll();
  },
  loadUserAll: function () {
    var that = this;
    user.user_all({
      openid: app.globalData.openid,
      task : this.data.pickerTask[this.data.search_task].value,
      level : this.data.pickerLevel[this.data.search_level].value,
      page: pager.next(),
      pageSize: pager.pageSize(),
      callback: function (data) {
        console.log("userSwitch:loadUserAll:size=" + data.data.length);
        if (that.data.userList && that.data.userList.length > 500) {
          that.data.userList = [];
        }
        var list = game.parseUserList(data.data, app.globalData.game.uid);
        that.setData({
          hasNext: pager.hasNext(data.data.length,data.total),
          pagerText: pager.text(),
          userList: my.concat(that.data.userList, list),
        });
        console.log("userSwitch:userList:size=" + that.data.userList.length);
      }
    });
  },

  doSwitch: function (e) {
    console.log("userSwitch:doSwitch:e=" + JSON.stringify(e));
    var uid = my.ival(e.currentTarget.dataset.uid);
    if (uid <= 0) {
      my.toast("错误：无效的UID");
      return;
    }
    my.L("user_data_uid", uid);
    my.L("selected_region", null);
    app.globalData.step = 0;
    wx.reLaunch({
      url: '../index/index',
    });
  },
  doLevelChange : function(e){
    console.log("value="+e.detail.value);
    this.setData({
      search_level : e.detail.value,
      userList:null
    });
    pager.init();
    this.loadUserAll();
  },
  doTaskChange: function (e) {
    console.log("value=" + e.detail.value);
    this.setData({
      search_task: e.detail.value,
      userList: null
    });
    pager.init();
    this.loadUserAll();
  },
  onShareAppMessage: app.share,

})